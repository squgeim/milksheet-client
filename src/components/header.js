import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '../shared-styles';

class Header extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        h1 {
          display: block;
          font-size: 1.5em;
          padding: 10px 5px;
          margin: 0;
          font-weight: normal;
        }
      </style>

      <h1><slot></slot></h1>
    `;
  }
}

if (!window.customElements.get('ms-header')) {
  window.customElements.define('ms-header', Header);
}
