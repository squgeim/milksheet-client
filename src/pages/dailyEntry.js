import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-listbox/paper-listbox.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/paper-item/paper-item-body.js';
import '@polymer/paper-item/paper-icon-item.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/paper-checkbox/paper-checkbox.js';
import '@polymer/paper-dropdown-menu/paper-dropdown-menu.js';
import '../shared-styles.js';

import '../components/header.js';

class DailyEntry extends PolymerElement {

  constructor() {
    super();

    this.fatRate = 4.77;
    this.snfRate = 2.53;

    this.merchants = [
      {"id":1,"name":"Burt Emrick"},
      {"id":2,"name":"Adrian Prentice"},
      {"id":3,"name":"Sena Loiacono"},
      {"id":4,"name":"Tamica Kaiser"},
      {"id":5,"name":"Maris Newbern"},
      {"id":6,"name":"Broderick Mayorga"},
      {"id":7,"name":"Elise Monier"},
      {"id":8,"name":"Reyna Srour"},
      {"id":9,"name":"Willow Thorpe"},
      {"id":10,"name":"Weldon Delk"},
      {"id":11,"name":"Kanesha Woolverton"},
      {"id":12,"name":"Beth Moses"},
      {"id":13,"name":"Karan Noble"},
      {"id":14,"name":"Lorrie Clausen"},
      {"id":15,"name":"Kennith Albrecht"},
      {"id":16,"name":"Delfina Bezanson"},
      {"id":17,"name":"Willian Poulos"},
      {"id":18,"name":"Vince Vibbert"},
      {"id":19,"name":"Lavona Irion"},
      {"id":20,"name":"Ariane Prime"}
    ];

    this.merchantInputValue = '';
    this.currentMerchant = {};
  }

  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }

        .grid {
          display: flex;
          justify-content: space-between;
        }

        .grid>div {
          flex: 0.49;
        }

        .list {
          border: 1px solid #ccc;
          max-height: 200px;
          overflow-y: auto;
        }

        .list paper-item {
          color: #333;
        }

        .buttons {
          margin-top: 20px;
        }
      </style>

      <div class="card">
        <ms-header>Daily Entry</ms-header>
        <div class="grid">
          <div>
            <paper-input
              value="{{date}}"
              label="Today"
              required="true"
              type="date"
            ></paper-input>
          </div>
          <div>
            <paper-dropdown-menu value="{{shift}}" label="Shift">
              <paper-listbox slot="dropdown-content" selected="0">
                <paper-item value="morning">Morning</paper-item>
                <paper-item value="evening">Evening</paper-item>
              </paper-listbox>
            </paper-dropdown-menu>
          </div>
        </div>
        <div class="grid">
          <div>
            <paper-input
              value="{{fatRate}}"
              required="true"
              label="Fat Rate"
              type="number">
            </paper-input>
          </div>
          <div>
            <paper-input
              value="{{snfRate}}"
              required="true"
              label="SNF Rate"
              type="number">
            </paper-input>
          </div>
        </div>
      </div>

      <div class="card">
        <paper-input
          value="{{merchantInputValue}}"
          label="Merchant"
          autofocus="true"
          required="true"
        ></paper-input>
        <div class="list">
          <template
            is="dom-repeat"
            items="[[merchants]]"
            filter="{{filterMerchantList(merchantInputValue)}}"
          >
            <paper-item
              active="[[item.isSelected]]"
              on-click="onSelectMerchant"
              value="[[item.id]]"
              role="menuitemradio"
            >
              [[item.name]]
            </paper-item>
          </template>
        </div>

        <div class="grid">
          <div>
            <paper-input value="{{formQuantity}}" label="Quantity (lt)" type="number"></paper-input>
          </div>
          <div>
            <paper-input value="{{formWaterMix}}" label="Water Mix (%)" type="number"></paper-input>
          </div>
        </div>
        <div class="grid">
          <div>
            <paper-input value="{{formFat}}" label="Fat (%)" type="number"></paper-input>
          </div>
          <div>
            <paper-input value="{{formSnf}}" label="SNF (%)" type="number"></paper-input>
          </div>
        </div>
        <div class="grid">
          <div>
            <paper-input value="{{formTransportationCost}}" label="Transportation Cost" type="number"></paper-input>
          </div>
        </div>

        <hr />

        <paper-input readonly label="Total Rate" value="[[totalRate]]"></paper-input>
        <paper-input readonly label="Total Amount" value="[[totalAmount]]"></paper-input>

        <hr />

        <div class="buttons">
          <paper-button raised class="primary-button" on-click="onSave">Save</paper-button>
          <paper-button on-click="onReset">Reset</paper-button>
        </div>
      </div>
    `;
  }

  static get properties() {
    return {
      fatRate: {
        type: Number,
        observer: '_formChanged',
      },
      snfRate: {
        type: Number,
        observer: '_formChanged',
      },
      formQuantity: {
        type: Number,
        observer: '_formChanged',
      },
      formWaterMix: {
        type: Number,
        observer: '_formChanged',
      },
      formFat: {
        type: Number,
        observer: '_formChanged',
      },
      formSnf: {
        type: Number,
        observer: '_formChanged',
      },
      formTransportationCost: {
        type: Number,
        observer: '_formChanged',
      },
    };
  }

  get date() {
    return new Date().toISOString().split('T')[0];
  }

  _formChanged() {
    const rate = this.fatRate * this.formFat + this.snfRate * this.formSnf;
    this.totalRate = rate - this.formTransportationCost;
    this.totalAmount = this.totalRate * this.formQuantity;
  }

  filterMerchantList(inputStr) {
    if (!inputStr) {
      return null
    }

    return item => {
      return item.name.toLowerCase().indexOf(inputStr.toLowerCase()) !== -1 || item.isSelected;
    };
  }

  onSelectMerchant(e) {
    const merchantId = e.target.value;

    this.merchants.forEach(element => {
      if (element.id === merchantId) {
        element.isSelected = true;
        this.merchantInputValue = element.name;
        this.currentMerchant = element;
      } else {
        element.isSelected = false;
      }
    });
  }

  onSave() {
    const entry = {
      date: this.date,
      shift: this.shift,
      fatRate: this.fatRate,
      snfRate: this.snfRate,
      merchantId: this.currentMerchant.id,
      merchantName: this.currentMerchant.name,
      quantity: this.formQuantity,
      waterMix: this.formWaterMix,
      fatMix: this.formFat,
      snfMix: this.formSnf,
      transportationCost: this.transportationCost,
    }

    console.log(entry);
  }

  onReset() {

  }
}

window.customElements.define('page-daily-entry', DailyEntry);
